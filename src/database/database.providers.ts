import { createConnection } from 'typeorm';
import { DBCONFIG } from './repository.const';

export const databaseProviders = [
    {
        provide: DBCONFIG.DATABASE_CONNECTION,
        useFactory: async () => await createConnection({
            type: 'mongodb',
            host: 'localhost',
            port: 27017,
            database: 'setel-fullstack',
            entities: [
                __dirname + '/../**/*.entity{.ts,.js}',
            ],
            synchronize: true,
        }),
    },
];
