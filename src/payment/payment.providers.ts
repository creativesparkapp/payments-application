import { Connection, Repository } from 'typeorm';
import { REPO, DBCONFIG } from '../database/repository.const';
import { Payment } from './entity/payment.entity';

export const paymentProviders = [
  {
    provide: REPO.PAYMENT_REPO,
    useFactory: (connection: Connection) => connection.getRepository(Payment),
    inject: [DBCONFIG.DATABASE_CONNECTION],
  },
];
