import { Injectable, Inject } from '@nestjs/common';
import { REPO } from '../database/repository.const';
import { Repository, UpdateResult } from 'typeorm';
import { Payment } from './entity/payment.entity';

@Injectable()
export class PaymentService {
    constructor(
        @Inject(REPO.PAYMENT_REPO)
        private readonly paymentRepository: Repository<Payment>,
    ) { }

    async createPayment(payment: Payment): Promise<Payment> {
        const existedPayment = await this.paymentRepository.findOne({ orderId: payment.orderId });
        if (!existedPayment) {
            return this.paymentRepository.save(payment);
        } else {
            Promise.reject('invalid request : payment existed');
        }
    }
}
