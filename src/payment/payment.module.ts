import { Module } from '@nestjs/common';
import { PaymentController } from './payment.controller';
import { PaymentService } from './payment.service';
import { paymentProviders } from './payment.providers';
import { PassportModule } from '@nestjs/passport';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [
    PassportModule,
    DatabaseModule,
  ],
  providers: [PaymentService, ...paymentProviders],
  exports: [PaymentService],
  controllers: [PaymentController],

})
export class PaymentModule { }
