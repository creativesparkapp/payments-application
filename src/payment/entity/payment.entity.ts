import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';

@Entity()
export class Payment {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    orderId: string;

    @Column()
    status: PaymentStatus;
}

export enum PaymentStatus {
    created = 'created',
    failed = 'failed',
    succeed = 'succeed',
}
