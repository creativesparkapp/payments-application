import { Controller, UseGuards, Post, Body, HttpException, HttpStatus } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { PaymentDto } from './dto/payment.dto';
import { PaymentService } from './payment.service';
import { Payment, PaymentStatus } from './entity/payment.entity';
import { SwaggerTag } from '../environment/swagger.const';
import * as  mongodb from 'mongodb';

@ApiUseTags(SwaggerTag.payment)
@Controller('payment')
export class PaymentController {
    constructor(private readonly paymentService: PaymentService) { }
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @Post('pay')
    async createPayment(@Body() paymentDto: PaymentDto) {
        const isSuccess = Math.random() < 0.6; // random success or fail payment with 60% success rate
        if (isSuccess) {
            const payment = new Payment();
            payment.orderId = new mongodb.ObjectId(paymentDto.orderId);
            payment.status = PaymentStatus.succeed;
            const createPayment = await this.paymentService.createPayment(payment);
            if (createPayment) {
                return new HttpException(createPayment.id, HttpStatus.OK);
            } else {
                return new HttpException('Invalid id', HttpStatus.BAD_REQUEST);
            }
        } else {
            return new HttpException('Transaction failed', HttpStatus.NOT_ACCEPTABLE);
        }
    }
}
