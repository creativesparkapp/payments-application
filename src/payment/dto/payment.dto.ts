import { ApiModelProperty } from '@nestjs/swagger';

export class PaymentDto {
    @ApiModelProperty()
    orderId: string;
}
