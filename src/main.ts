import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { SwaggerTag } from './environment/swagger.const';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('setel-fullstack (PAYMENT)')
    .setDescription('Microservice for payment module')
    .setVersion('DEMO')
    .addBearerAuth('authorization')
    .addTag(SwaggerTag.payment)
    .build();
  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('api', app, document);
  await app.listen(3002);
}
bootstrap();
