import { ObjectID } from 'typeorm';

export class JwtPayloadDto {
    uid: ObjectID;
}
