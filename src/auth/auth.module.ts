import { Module } from '@nestjs/common';
import { JwtStrategy } from './stratergy/jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [
    DatabaseModule,
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1days' },
    }),
  ],
  providers: [JwtStrategy],
})
export class AuthModule { }
