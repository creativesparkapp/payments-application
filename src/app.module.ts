import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from './database/database.module';
import { PassportModule } from '@nestjs/passport';
import { PaymentModule } from './payment/payment.module';

@Module({
  imports: [AuthModule, DatabaseModule, PassportModule, PaymentModule],
  providers: [AppService],
})
export class AppModule { }
